package com.abdeljalilkchih.learnarabicforkids;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Abdeljalil on 22/02/2018.
 */

public class cTextView extends TextView {
    public cTextView(Context context) {
        super(context);
        init();
    }

    public cTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public cTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/cairobold.ttf");

        setTypeface(typeface);
    }
}
