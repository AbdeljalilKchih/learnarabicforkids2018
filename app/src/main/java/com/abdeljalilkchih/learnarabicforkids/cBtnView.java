package com.abdeljalilkchih.learnarabicforkids;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by Abdeljalil on 24/02/2018.
 */

public class cBtnView extends Button {
    public cBtnView(Context context) {
        super(context);
        init();
    }

    public cBtnView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public cBtnView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/cairobold.ttf");
        setTypeface(typeface);
    }

}
