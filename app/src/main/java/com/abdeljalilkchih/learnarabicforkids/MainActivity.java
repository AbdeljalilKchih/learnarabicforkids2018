package com.abdeljalilkchih.learnarabicforkids;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.Button;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.onesignal.OneSignal;

public class MainActivity extends AppCompatActivity {
    private AdView myAdViewz;
    private SeekBar volumeSeekbar = null;
    private AudioManager audioManager = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        setContentView(R.layout.activity_main);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-9961705383410701~5674929633");
        myAdViewz = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        myAdViewz.loadAd(adRequest);

    }

    public void Btn_Next(View view) {
        Intent MyIntend1 = new Intent(this,LearnMain2Activity.class);
        startActivity(MyIntend1);
    }

    public void PowerClick(View MyView1) {
        final Dialog dialog2 = new Dialog(this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog2.setContentView(R.layout.seekbar);
        Button Closebtn = (Button) dialog2.findViewById(R.id.Cls);
        Closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.setCancelable(false);
        dialog2.show();
        try
        {
            volumeSeekbar = (SeekBar)dialog2.findViewById(R.id.Volume_control);
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            volumeSeekbar.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            volumeSeekbar.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));


            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onStopTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
                {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void Btn_Exit(View view) {
        final Dialog dialogX = new Dialog(this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialogX.setContentView(R.layout.activity_menu);
        Button yesbtn1 = (Button)dialogX.findViewById(R.id.OKButton);
        Button nobtn2 = (Button)dialogX.findViewById(R.id.NOButton);
        TextView tvd1 = (TextView)dialogX.findViewById(R.id.tv1);
        TextView tvd2 = (TextView)dialogX.findViewById(R.id.tv2);
        dialogX.setCancelable(false);
        dialogX.show();
        yesbtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.finish();
                finish();
                System.exit(0);
            }
        });
        nobtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogX.dismiss();
                Toast myToast = new Toast(MainActivity.this);
                myToast.setDuration(Toast.LENGTH_LONG);
                myToast.setGravity(Gravity.CENTER,0,0);
                LayoutInflater MyLF = getLayoutInflater();
                View MyView1 = MyLF.inflate(R.layout.toast_welcome,(ViewGroup)findViewById(R.id.Linear_Layout));
                myToast.setView(MyView1);
                myToast.show();
            }
        });
    }

    public void Btn_share(View view) {
        String shareText ="تطبيق معلم الحروف العربية";
        String shareLinkapp="https://play.google.com/store/apps/details?id=com.abdeljalilkchih.learnarabicforkids";
        String towShare = shareText +"\n" + shareLinkapp;
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,towShare);
        startActivity(shareIntent);
    }
}
