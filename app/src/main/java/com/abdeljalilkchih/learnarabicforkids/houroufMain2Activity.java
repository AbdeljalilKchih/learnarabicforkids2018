package com.abdeljalilkchih.learnarabicforkids;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class houroufMain2Activity extends AppCompatActivity {
    int nmzer = 0;
    int klmlpSout=0;
    ImageView MyimgV0;
    ImageView MyimgV1;
    ImageView MyimgV2;
    ImageView MyimgV3;
    ImageView MyimgV4;
    ImageView MyimgV5;
    ImageView MyimgV6;
    ImageView MyimgVX;
    ImageView MyimgVY;
    ImageView MyimgVZ;
    Button btn_back;
    Button btn_next;
    MediaPlayer MyPlayer;
    int [] nature = new int [] {R.drawable.a1,R.drawable.b1,R.drawable.c1,R.drawable.d1,R.drawable.e1,R.drawable.f1,R.drawable.g1,R.drawable.h1,R.drawable.i1,R.drawable.j1,
            R.drawable.k1,R.drawable.l1,R.drawable.m1,R.drawable.n1,R.drawable.o1,R.drawable.p1,R.drawable.q1,R.drawable.r1,R.drawable.s1,R.drawable.t1,R.drawable.u1,R.drawable.v1,
            R.drawable.w1,R.drawable.x1,R.drawable.y1,R.drawable.z1,R.drawable.za1,R.drawable.zb1};
    int [] mature = new int [] {R.drawable.a2,R.drawable.b2,R.drawable.c2,R.drawable.d2,R.drawable.e2,R.drawable.f2,R.drawable.g2,R.drawable.h2,R.drawable.i2,R.drawable.j2,
            R.drawable.k2,R.drawable.l2,R.drawable.m2,R.drawable.n2,R.drawable.o2,R.drawable.p2,R.drawable.q2,R.drawable.r2,R.drawable.s2,R.drawable.t2,R.drawable.u2,R.drawable.v2,
            R.drawable.w2,R.drawable.x2,R.drawable.y2,R.drawable.z2,R.drawable.za2,R.drawable.zb2,};
    int [] kalima = new int [] {R.drawable.kalima1,R.drawable.kalima2,R.drawable.kalima3,R.drawable.kalima4,R.drawable.kalima5,R.drawable.kalima6,R.drawable.kalima7,R.drawable.kalima8,R.drawable.kalima9,R.drawable.kalima10,R.drawable.kalima11,R.drawable.kalima12,R.drawable.kalima13,R.drawable.kalima14,R.drawable.kalima15,R.drawable.kalima16,R.drawable.kalima17,R.drawable.kalima18,R.drawable.kalima19,R.drawable.kalima20,R.drawable.kalima21,R.drawable.kalima22,R.drawable.kalima23,R.drawable.kalima24,R.drawable.kalima25,R.drawable.kalima26,R.drawable.kalima27,R.drawable.kalima28};
   int [] kalimaAS = new int[]{R.raw.kalima1,R.raw.kalima2,R.raw.kalima3,R.raw.kalima4,R.raw.kalima5,R.raw.kalima6,R.raw.kalima7,R.raw.kalima8,R.raw.kalima9,R.raw.kalima10,R.raw.kalima11,R.raw.kalima12,R.raw.kalima13,R.raw.kalima14,R.raw.kalima15,R.raw.kalima16,R.raw.kalima17,R.raw.kalima18,R.raw.kalima19,R.raw.kalima20,R.raw.kalima21,R.raw.kalima22,R.raw.kalima23,R.raw.kalima24,R.raw.kalima25,R.raw.kalima26,R.raw.kalima27,R.raw.kalima28};
    int [] kalimaASM = new int[]{R.raw.kalima1,R.raw.kalima2,R.raw.kalima3,R.raw.kalima4,R.raw.kalima5,R.raw.kalima6,R.raw.kalima7,R.raw.kalima8,R.raw.kalima9,R.raw.kalima10,R.raw.kalima11,R.raw.kalima12,R.raw.kalima13,R.raw.kalima14,R.raw.kalima15,R.raw.kalima16,R.raw.kalima17,R.raw.kalima18,R.raw.kalima19,R.raw.kalima20,R.raw.kalima21,R.raw.kalima22,R.raw.kalima23,R.raw.kalima24,R.raw.kalima25,R.raw.kalima26,R.raw.kalima27,R.raw.kalima28};
    int [] letterpic = new int[] {R.drawable.pic1,R.drawable.pic2,R.drawable.pic3,R.drawable.pic4,R.drawable.pic5,R.drawable.pic6,R.drawable.pic7,R.drawable.pic8,R.drawable.pic9,R.drawable.pic10,R.drawable.pic11,R.drawable.pic12,R.drawable.pic13,R.drawable.pic14,R.drawable.pic15,R.drawable.pic16,R.drawable.pic17,R.drawable.pic18,R.drawable.pic19,R.drawable.pic20,R.drawable.pic21,R.drawable.pic22,R.drawable.pic23,R.drawable.pic24,R.drawable.pic25,R.drawable.pic26,R.drawable.pic27,R.drawable.pic28};
    int [] kalimaAll3_1 = new int[] {R.drawable.kalimo1,R.drawable.kalimu1,R.drawable.kalima1};
    int [] kalimaAllA3_1 = new int[] {R.raw.kalimo1,R.raw.kalimu1,R.raw.kalima1};
    int [] kalimaAllA3_2 = new int[] {R.raw.kalimo2,R.raw.kalimu2,R.raw.kalima2};
    int [] kalimaAllA3_3 = new int[] {R.raw.kalimo3,R.raw.kalimu3,R.raw.kalima3};
    int [] kalimaAllA3_4 = new int[] {R.raw.kalimo4,R.raw.kalimu4,R.raw.kalima4};
    int [] kalimaAllA3_5 = new int[] {R.raw.kalimo5,R.raw.kalimu5,R.raw.kalima5};
    int [] kalimaAllA3_6 = new int[] {R.raw.kalimo6,R.raw.kalimu6,R.raw.kalima6};
    int [] kalimaAllA3_7 = new int[] {R.raw.kalimo7,R.raw.kalimu7,R.raw.kalima7};
    int [] kalimaAllA3_8 = new int[] {R.raw.kalimo8,R.raw.kalimu8,R.raw.kalima8};
    int [] kalimaAllA3_9 = new int[] {R.raw.kalimo9,R.raw.kalimu9,R.raw.kalima9};
    int [] kalimaAllA3_10 = new int[] {R.raw.kalimo10,R.raw.kalimu10,R.raw.kalima10};
    int [] kalimaAllA3_11 = new int[] {R.raw.kalimo11,R.raw.kalimu11,R.raw.kalima11};
    int [] kalimaAllA3_12 = new int[] {R.raw.kalimo12,R.raw.kalimu12,R.raw.kalima12};
    int [] kalimaAllA3_13 = new int[] {R.raw.kalimo13,R.raw.kalimu13,R.raw.kalima13};
    int [] kalimaAllA3_14 = new int[] {R.raw.kalimo14,R.raw.kalimu14,R.raw.kalima14};
    int [] kalimaAllA3_15 = new int[] {R.raw.kalimo15,R.raw.kalimu15,R.raw.kalima15};
    int [] kalimaAllA3_16 = new int[] {R.raw.kalimo16,R.raw.kalimu16,R.raw.kalima16};
    int [] kalimaAllA3_17 = new int[] {R.raw.kalimo17,R.raw.kalimu17,R.raw.kalima17};
    int [] kalimaAllA3_18 = new int[] {R.raw.kalimo18,R.raw.kalimu18,R.raw.kalima18};
    int [] kalimaAllA3_19 = new int[] {R.raw.kalimo19,R.raw.kalimu19,R.raw.kalima19};
    int [] kalimaAllA3_20 = new int[] {R.raw.kalimo20,R.raw.kalimu20,R.raw.kalima20};
    int [] kalimaAllA3_21 = new int[] {R.raw.kalimo21,R.raw.kalimu21,R.raw.kalima21};
    int [] kalimaAllA3_22 = new int[] {R.raw.kalimo22,R.raw.kalimu22,R.raw.kalima22};
    int [] kalimaAllA3_23 = new int[] {R.raw.kalimo23,R.raw.kalimu23,R.raw.kalima23};
    int [] kalimaAllA3_24 = new int[] {R.raw.kalimo24,R.raw.kalimu24,R.raw.kalima24};
    int [] kalimaAllA3_25 = new int[] {R.raw.kalimo25,R.raw.kalimu25,R.raw.kalima25};
    int [] kalimaAllA3_26 = new int[] {R.raw.kalimo26,R.raw.kalimu26,R.raw.kalima26};
    int [] kalimaAllA3_27 = new int[] {R.raw.kalimo27,R.raw.kalimu27,R.raw.kalima27};
    int [] kalimaAllA3_28 = new int[] {R.raw.kalimo28,R.raw.kalimu28,R.raw.kalima28};
    int [] kalimaAll3_2 = new int[] {R.drawable.kalimo2,R.drawable.kalimu2,R.drawable.kalima2};
    int [] kalimaAll3_3 = new int[] {R.drawable.kalimo3,R.drawable.kalimu3,R.drawable.kalima3};
    int [] kalimaAll3_4 = new int[] {R.drawable.kalimo4,R.drawable.kalimu4,R.drawable.kalima4};
    int [] kalimaAll3_5 = new int[] {R.drawable.kalimo5,R.drawable.kalimu5,R.drawable.kalima5};
    int [] kalimaAll3_6 = new int[] {R.drawable.kalimo6,R.drawable.kalimu6,R.drawable.kalima6};
    int [] kalimaAll3_7 = new int[] {R.drawable.kalimo7,R.drawable.kalimu7,R.drawable.kalima7};
    int [] kalimaAll3_8 = new int[] {R.drawable.kalimo8,R.drawable.kalimu8,R.drawable.kalima8};
    int [] kalimaAll3_9 = new int[] {R.drawable.kalimo9,R.drawable.kalimu9,R.drawable.kalima9};
    int [] kalimaAll3_10 = new int[] {R.drawable.kalimo10,R.drawable.kalimu10,R.drawable.kalima10};
    int [] kalimaAll3_11 = new int[] {R.drawable.kalimo11,R.drawable.kalimu11,R.drawable.kalima11};
    int [] kalimaAll3_12 = new int[] {R.drawable.kalimo12,R.drawable.kalimu12,R.drawable.kalima12};
    int [] kalimaAll3_13 = new int[] {R.drawable.kalimo13,R.drawable.kalimu13,R.drawable.kalima13};
    int [] kalimaAll3_14 = new int[] {R.drawable.kalimo14,R.drawable.kalimu14,R.drawable.kalima14};
    int [] kalimaAll3_15 = new int[] {R.drawable.kalimo15,R.drawable.kalimu15,R.drawable.kalima15};
    int [] kalimaAll3_16 = new int[] {R.drawable.kalimo16,R.drawable.kalimu16,R.drawable.kalima16};
    int [] kalimaAll3_17 = new int[] {R.drawable.kalimo17,R.drawable.kalimu17,R.drawable.kalima17};
    int [] kalimaAll3_18 = new int[] {R.drawable.kalimo18,R.drawable.kalimu18,R.drawable.kalima18};
    int [] kalimaAll3_19 = new int[] {R.drawable.kalimo19,R.drawable.kalimu19,R.drawable.kalima19};
    int [] kalimaAll3_20 = new int[] {R.drawable.kalimo20,R.drawable.kalimu20,R.drawable.kalima20};
    int [] kalimaAll3_21 = new int[] {R.drawable.kalimo21,R.drawable.kalimu21,R.drawable.kalima21};
    int [] kalimaAll3_22 = new int[] {R.drawable.kalimo22,R.drawable.kalimu22,R.drawable.kalima22};
    int [] kalimaAll3_23 = new int[] {R.drawable.kalimo23,R.drawable.kalimu23,R.drawable.kalima23};
    int [] kalimaAll3_24 = new int[] {R.drawable.kalimo24,R.drawable.kalimu24,R.drawable.kalima24};
    int [] kalimaAll3_25 = new int[] {R.drawable.kalimo25,R.drawable.kalimu25,R.drawable.kalima25};
    int [] kalimaAll3_26 = new int[] {R.drawable.kalimo26,R.drawable.kalimu26,R.drawable.kalima26};
    int [] kalimaAll3_27 = new int[] {R.drawable.kalimo27,R.drawable.kalimu27,R.drawable.kalima27};
    int [] kalimaAll3_28 = new int[] {R.drawable.kalimo28,R.drawable.kalimu28,R.drawable.kalima28};
    int [] letterpicAll3_1 = new int[]{R.drawable.pico1,R.drawable.picu1,R.drawable.pic1};
    int [] letterpicAll3_2 = new int[]{R.drawable.pico2,R.drawable.picu2,R.drawable.pic2};
    int [] letterpicAll3_3 = new int[]{R.drawable.pico3,R.drawable.picu3,R.drawable.pic3};
    int [] letterpicAll3_4 = new int[]{R.drawable.pico4,R.drawable.picu4,R.drawable.pic4};
    int [] letterpicAll3_5 = new int[]{R.drawable.pico5,R.drawable.picu5,R.drawable.pic5};
    int [] letterpicAll3_6 = new int[]{R.drawable.pico6,R.drawable.picu6,R.drawable.pic6};
    int [] letterpicAll3_7 = new int[]{R.drawable.pico7,R.drawable.picu7,R.drawable.pic7};
    int [] letterpicAll3_8 = new int[]{R.drawable.pico8,R.drawable.picu8,R.drawable.pic8};
    int [] letterpicAll3_9 = new int[]{R.drawable.pico9,R.drawable.picu9,R.drawable.pic9};
    int [] letterpicAll3_10 = new int[]{R.drawable.pico10,R.drawable.picu10,R.drawable.pic10};
    int [] letterpicAll3_11 = new int[]{R.drawable.pico11,R.drawable.picu11,R.drawable.pic11};
    int [] letterpicAll3_12 = new int[]{R.drawable.pico12,R.drawable.picu12,R.drawable.pic12};
    int [] letterpicAll3_13 = new int[]{R.drawable.pico13,R.drawable.picu13,R.drawable.pic13};
    int [] letterpicAll3_14 = new int[]{R.drawable.pico14,R.drawable.picu14,R.drawable.pic14};
    int [] letterpicAll3_15 = new int[]{R.drawable.pico15,R.drawable.picu15,R.drawable.pic15};
    int [] letterpicAll3_16 = new int[]{R.drawable.pico16,R.drawable.picu16,R.drawable.pic16};
    int [] letterpicAll3_17 = new int[]{R.drawable.pico17,R.drawable.picu17,R.drawable.pic17};
    int [] letterpicAll3_18 = new int[]{R.drawable.pico18,R.drawable.picu18,R.drawable.pic18};
    int [] letterpicAll3_19 = new int[]{R.drawable.pico19,R.drawable.picu19,R.drawable.pic19};
    int [] letterpicAll3_20 = new int[]{R.drawable.pico20,R.drawable.picu20,R.drawable.pic20};
    int [] letterpicAll3_21 = new int[]{R.drawable.pico21,R.drawable.picu21,R.drawable.pic21};
    int [] letterpicAll3_22 = new int[]{R.drawable.pico22,R.drawable.picu22,R.drawable.pic22};
    int [] letterpicAll3_23 = new int[]{R.drawable.pico23,R.drawable.picu23,R.drawable.pic23};
    int [] letterpicAll3_24 = new int[]{R.drawable.pico24,R.drawable.picu24,R.drawable.pic24};
    int [] letterpicAll3_25 = new int[]{R.drawable.pico25,R.drawable.picu25,R.drawable.pic25};
    int [] letterpicAll3_26 = new int[]{R.drawable.pico26,R.drawable.picu26,R.drawable.pic26};
    int [] letterpicAll3_27 = new int[]{R.drawable.pico27,R.drawable.picu27,R.drawable.pic27};
    int [] letterpicAll3_28 = new int[]{R.drawable.pico28,R.drawable.picu28,R.drawable.pic28};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hourouf_main2);
        MyimgV0 = (ImageView) findViewById(R.id.IMGV0);
        MyimgV1 = (ImageView) findViewById(R.id.IMGV1);
        MyimgV2 = (ImageView) findViewById(R.id.IMGV2);
        MyimgV3 = (ImageView) findViewById(R.id.IMGV3);
        MyimgV4 = (ImageView) findViewById(R.id.IMGV4);
        MyimgV5 = (ImageView) findViewById(R.id.IMGV5);
        MyimgV6 = (ImageView) findViewById(R.id.IMGV6);
        MyimgVX = (ImageView) findViewById(R.id.IMGVX);
        MyimgVY = (ImageView) findViewById(R.id.IMGVY);
        MyimgVZ = (ImageView) findViewById(R.id.IMGVZ);
        btn_back = (Button) findViewById(R.id.BT1);
        btn_next = (Button) findViewById(R.id.BT2);
        MyimgVX.setImageResource(letterpic[nmzer]);
        MyimgVZ.setImageResource(kalima[nmzer]);
        MyimgV0.setImageResource(nature[nmzer]);
        MyimgV6.setImageResource(mature[nmzer]);
        MyPlayer = MediaPlayer.create(this,kalimaAS[nmzer]);
        MyPlayer.start();
    }

    public void btn_home(View view) {
        finish();
        houroufMain2Activity.this.finish();
    }

    public void mybtn_next(View view) {
        try {
            if(nmzer<mature.length-1) {nmzer++;}
            String txt=String.valueOf(nmzer+1);
            if (nmzer<=8)
            {
                Toast.makeText(this,"الحرف رقم:"+txt, Toast.LENGTH_SHORT).show();
            }
            else {Toast.makeText(this,"الحرف عدد:"+txt , Toast.LENGTH_SHORT).show();}
            if((nmzer<mature.length)) {
                if (nmzer<nature.length-5) {
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);
                    MyimgV0.setImageResource(nature[nmzer]);
                    MyimgV1.setImageResource(nature[nmzer+5]);
                    MyimgV2.setImageResource(nature[nmzer+4]);
                    MyimgV3.setImageResource(nature[nmzer+3]);
                    MyimgV4.setImageResource(nature[nmzer+2]);
                    MyimgV5.setImageResource(nature[nmzer+1]);
                    MyimgV6.setImageResource(mature[nmzer]);
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                }
                else if (nmzer==nature.length-5)  {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);
                    MyimgV1.setImageResource(nature[nmzer+4]);
                    MyimgV2.setImageResource(nature[nmzer+3]);
                    MyimgV3.setImageResource(nature[nmzer+2]);
                    MyimgV4.setImageResource(nature[nmzer+1]);
                    MyimgV5.setImageResource(mature[nmzer]);
                    MyimgV6.setImageResource(nature[nmzer-1]);
                    MyimgV0.setImageResource(nature[nmzer]);}
                else if (nmzer==nature.length-4) {
                    MyPlayer = MediaPlayer.create(this,kalimaAS[nmzer-1]);
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);MyimgV1.setImageResource(nature[nmzer+3]);MyimgV2.setImageResource(nature[nmzer+2]);MyimgV3.setImageResource(nature[nmzer+1]);MyimgV4.setImageResource(mature[nmzer]);MyimgV5.setImageResource(nature[nmzer-1]);MyimgV6.setImageResource(nature[nmzer-2]);MyimgV0.setImageResource(nature[nmzer]);}
                else if (nmzer==nature.length-3) {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);MyimgV1.setImageResource(nature[nmzer+2]);MyimgV2.setImageResource(nature[nmzer+1]);MyimgV3.setImageResource(mature[nmzer]);MyimgV4.setImageResource(nature[nmzer-1]);;MyimgV5.setImageResource(nature[nmzer-2]);MyimgV6.setImageResource(nature[nmzer-3]);MyimgV0.setImageResource(nature[nmzer]);}
                else if (nmzer==nature.length-2) {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);MyimgV1.setImageResource(nature[nmzer+1]);MyimgV2.setImageResource(mature[nmzer]);MyimgV3.setImageResource(nature[nmzer-1]);MyimgV4.setImageResource(nature[nmzer-2]);;MyimgV5.setImageResource(nature[nmzer-3]);MyimgV6.setImageResource(nature[nmzer-4]);MyimgV0.setImageResource(nature[nmzer]);}
                else if (nmzer==nature.length-1) {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);MyimgV1.setImageResource(mature[nmzer]);MyimgV2.setImageResource(nature[nmzer-1]);MyimgV3.setImageResource(nature[nmzer-2]);MyimgV4.setImageResource(nature[nmzer-3]);;MyimgV5.setImageResource(nature[nmzer-4]);MyimgV6.setImageResource(nature[nmzer-5]);MyimgV0.setImageResource(nature[nmzer]);}
            }

        }
        catch(Exception ex) {ex.getMessage();}
    }
    public void mybtn_back(View view) {
        try {
            if (nmzer>0) {
                nmzer--;
            }
            String txt = String.valueOf(nmzer+1);
            if (nmzer<=8)
            {
                Toast.makeText(this,"الحرف رقم:"+txt, Toast.LENGTH_SHORT).show();
            }
            else {Toast.makeText(this,"الحرف عدد:"+txt, Toast.LENGTH_SHORT).show();}
            if ((nmzer>=0)){
                if ((nmzer >= 5))  {
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);
                    MyimgV0.setImageResource(nature[nmzer]);
                    MyimgV1.setImageResource(mature[nmzer]);
                    MyimgV2.setImageResource(nature[nmzer - 1]);
                    MyimgV3.setImageResource(nature[nmzer - 2]);
                    MyimgV4.setImageResource(nature[nmzer - 3]);
                    MyimgV5.setImageResource(nature[nmzer - 4]);
                    MyimgV6.setImageResource(nature[nmzer - 5]);
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                } else if (nmzer == 4) {
                    MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);
                    MyimgV0.setImageResource(nature[nmzer]);
                    MyimgV1.setImageResource(nature[nmzer+1]);
                    MyimgV2.setImageResource(mature[nmzer]);
                    MyimgV3.setImageResource(nature[nmzer - 1]);
                    MyimgV4.setImageResource(nature[nmzer - 2]);
                    MyimgV5.setImageResource(nature[nmzer - 3]);
                    MyimgV6.setImageResource(nature[nmzer - 4]);
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaAS[nmzer]);
                    MyPlayer.start();
                }
                else if (nmzer == 3) {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgV1.setImageResource(nature[nmzer+2]); MyimgV2.setImageResource(nature[nmzer+1]);MyimgV3.setImageResource(mature[nmzer]);MyimgV4.setImageResource(nature[nmzer - 1]);MyimgV5.setImageResource(nature[nmzer - 2]);MyimgV6.setImageResource(nature[nmzer - 3]);MyimgV0.setImageResource(nature[nmzer]); MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);}
                else if (nmzer == 2) {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgV1.setImageResource(nature[nmzer+3]);MyimgV2.setImageResource(nature[nmzer+2]);MyimgV3.setImageResource(nature[nmzer+1]);MyimgV4.setImageResource(mature[nmzer]); MyimgV5.setImageResource(nature[nmzer - 1]);MyimgV6.setImageResource(nature[nmzer - 2]);MyimgV0.setImageResource(nature[nmzer]); MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);}
                else if (nmzer == 1) {
                    if (MyPlayer.isPlaying()) {
                        MyPlayer.stop();
                        MyPlayer.release();
                    }
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgV1.setImageResource(nature[nmzer+4]);MyimgV2.setImageResource(nature[nmzer+3]);MyimgV3.setImageResource(nature[nmzer+2]);MyimgV4.setImageResource(nature[nmzer+1]);MyimgV5.setImageResource(mature[nmzer]);MyimgV6.setImageResource(nature[nmzer - 1]); MyimgV0.setImageResource(nature[nmzer]); MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);}
                else if (nmzer == 0) {
                    MyPlayer = MediaPlayer.create(this,kalimaASM[nmzer]);
                    MyPlayer.start();
                    MyimgV1.setImageResource(nature[nmzer+5]);MyimgV2.setImageResource(nature[nmzer+4]);MyimgV3.setImageResource(nature[nmzer+3]);MyimgV4.setImageResource(nature[nmzer+2]);MyimgV5.setImageResource(nature[nmzer+1]);MyimgV6.setImageResource(mature[nmzer]); MyimgV0.setImageResource(nature[nmzer]); MyimgVX.setImageResource(letterpic[nmzer]);
                    MyimgVZ.setImageResource(kalima[nmzer]);}}

        }
        catch(Exception ex) {ex.getMessage();}}

    public void Next_Image(View view) {
        if (nmzer==0)
        {
            MyimgVZ.setImageResource(kalimaAll3_1[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_1[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_1[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;

            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==1)
        {
            MyimgVZ.setImageResource(kalimaAll3_2[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_2[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_2[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;

            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==2)
        {
            MyimgVZ.setImageResource(kalimaAll3_3[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_3[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_3[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==3)
        {
            MyimgVZ.setImageResource(kalimaAll3_4[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_4[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_4[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==4)
        {
            MyimgVZ.setImageResource(kalimaAll3_5[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_5[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_5[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==5)
        {
            MyimgVZ.setImageResource(kalimaAll3_6[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_6[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_6[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==6)
        {
            MyimgVZ.setImageResource(kalimaAll3_7[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_7[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_7[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==7)
        {
            MyimgVZ.setImageResource(kalimaAll3_8[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_8[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_8[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==8)
        {
            MyimgVZ.setImageResource(kalimaAll3_9[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_9[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_9[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==9)
        {
            MyimgVZ.setImageResource(kalimaAll3_10[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_10[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_10[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==10)
        {
            MyimgVZ.setImageResource(kalimaAll3_11[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_11[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_11[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==11)
        {
            MyimgVZ.setImageResource(kalimaAll3_12[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_12[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_12[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==12)
        {
            MyimgVZ.setImageResource(kalimaAll3_13[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_13[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_13[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==13)
        {
            MyimgVZ.setImageResource(kalimaAll3_14[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_14[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_14[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==14)
        {
            MyimgVZ.setImageResource(kalimaAll3_15[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_15[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_15[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==15)
        {
            MyimgVZ.setImageResource(kalimaAll3_16[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_16[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_16[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==16)
        {
            MyimgVZ.setImageResource(kalimaAll3_17[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_17[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_17[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==17)
        {
            MyimgVZ.setImageResource(kalimaAll3_18[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_18[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_18[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if(klmlpSout>2) {klmlpSout=0;}
        }
        if (nmzer==18)
        {
            MyimgVZ.setImageResource(kalimaAll3_19[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_19[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_19[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==19)
        {
            MyimgVZ.setImageResource(kalimaAll3_20[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_20[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_20[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==20)
        {
            MyimgVZ.setImageResource(kalimaAll3_21[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_21[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_21[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==21)
        {
            MyimgVZ.setImageResource(kalimaAll3_22[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_22[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_22[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==22)
        {
            MyimgVZ.setImageResource(kalimaAll3_23[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_23[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_23[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==23)
        {
            MyimgVZ.setImageResource(kalimaAll3_24[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_24[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_24[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==24)
        {
            MyimgVZ.setImageResource(kalimaAll3_25[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_25[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_25[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==25)
        {
            MyimgVZ.setImageResource(kalimaAll3_26[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_26[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_26[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==26)
        {
            MyimgVZ.setImageResource(kalimaAll3_27[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_27[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_27[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
        if (nmzer==27)
        {
            MyimgVZ.setImageResource(kalimaAll3_28[klmlpSout]);
            MyimgVX.setImageResource(letterpicAll3_28[klmlpSout]);
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(this,kalimaAllA3_28[klmlpSout]);
            MyPlayer.start();
            klmlpSout++;
            if((klmlpSout>2)) {klmlpSout=0;}
        }
    }


}









