package com.abdeljalilkchih.learnarabicforkids;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainSouraActivity extends AppCompatActivity implements RewardedVideoAdListener {
    private RewardedVideoAd myAdViewer;
    CountDownTimer MyTimer1,MyTimer2,MyTimer3,MyTimer4,MyTimer5,MyTimer6,MyTimer7,MyTimer8;
    boolean isRunning1 = false;
    boolean isRunning2 = false;
    boolean isRunning3 = false;
    boolean isRunning4 = false;
    boolean isRunning5 = false;
    boolean isRunning6 = false;
    boolean isRunning7 = false;
    boolean isRunning8 = false;
    Animation anim ;
    ImageView BoutonX1s;
    ImageView BoutonX2s;
    ImageView BoutonX3s;
    ImageView BoutonX4s;
    ImageView BoutonX5s;
    ImageView BoutonX6s;
    ImageView BoutonX7s;
    ImageView BoutonX8s;
    ImageView BoutonX9s;
    ImageView BoutonX10s;
    ImageView Test1es;
    ImageView Test2es;
    ImageView Test3es;
    ImageView TesTezDialog;
    TextView Questions,QuestionsDialog;
    LinearLayout Tableau ;
    MyDbConnexion db = new MyDbConnexion(this);
    List<Model> Listmodel = new ArrayList<>();
    Model nawModel = new Model();
    Model nawModel0 = new Model();
    Model nawModel1 = new Model();
    Model nawModel2 = new Model();
    Model nawModel3 = new Model();
    Model nawModel4 = new Model();
    Model nawModel5 = new Model();
    Model nawModel6 = new Model();
    Model nawModel7 = new Model();
    Model nawModel8 = new Model();
    Model nawModel9 = new Model();
    Model nawModel10 = new Model();
    MediaPlayer MyPlayer;
    RoundCornerProgressBar progress_bar;
    int h = 0;
    int i = 0;
    int sizeAilt = 0;
    float x = 0;
    float y = 0;
    public int[]faux = {R.raw.faux1, R.raw.faux2, R.raw.faux3, R.raw.faux4};
    public int[]vrai = {R.raw.vrai1, R.raw.vrai2};

    @Override
    public void onDestroy(){
        myAdViewer.destroy(this);
        super.onDestroy();
    }
    @Override
    public void onPause(){
        myAdViewer.pause(this);
        super.onPause();
    }
    @Override
public void onResume(){
  myAdViewer.resume(this);
  super.onResume();
}
    @Override
    public void onStop(){
        if (isRunning1) {MyTimer1.cancel();}
        if (isRunning2) {MyTimer2.cancel();}
        if (isRunning3){ MyTimer3.cancel();}
        if (isRunning4){ MyTimer4.cancel();}
        if (isRunning5){ MyTimer5.cancel();}
        if (isRunning6){ MyTimer6.cancel();}
        if (isRunning7){ MyTimer7.cancel();}
        if (isRunning8){MyTimer8.cancel();}
        if (MyPlayer.isPlaying()) { MyPlayer.stop();MyPlayer.release(); }
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_soura);
        BoutonX1s = (ImageView) findViewById(R.id.BoutonX1);
        BoutonX2s = (ImageView) findViewById(R.id.BoutonX2);
        BoutonX3s = (ImageView) findViewById(R.id.BoutonX3);
        BoutonX4s = (ImageView) findViewById(R.id.BoutonX4);
        BoutonX5s = (ImageView) findViewById(R.id.BoutonX5);
        BoutonX6s = (ImageView) findViewById(R.id.BoutonX6);
        BoutonX7s = (ImageView) findViewById(R.id.BoutonX7);
        BoutonX8s = (ImageView) findViewById(R.id.BoutonX8);
        BoutonX9s = (ImageView) findViewById(R.id.BoutonX9);
        BoutonX10s = (ImageView) findViewById(R.id.BoutonX10);
        BoutonX1s.setVisibility(View.INVISIBLE);
        BoutonX2s.setVisibility(View.INVISIBLE);
        BoutonX3s.setVisibility(View.INVISIBLE);
        BoutonX4s.setVisibility(View.INVISIBLE);
        BoutonX5s.setVisibility(View.INVISIBLE);
        BoutonX6s.setVisibility(View.INVISIBLE);
        BoutonX7s.setVisibility(View.INVISIBLE);
        BoutonX8s.setVisibility(View.INVISIBLE);
        BoutonX9s.setVisibility(View.INVISIBLE);
        BoutonX10s.setVisibility(View.INVISIBLE);
        ///Progress_bar Declaration
        progress_bar = (RoundCornerProgressBar) findViewById(R.id.DeterminateBar);
        progress_bar.setMax(100);
        //Question Text
        Questions = (TextView) findViewById(R.id.Question);
        // three pics for test
        Test1es = (ImageView) findViewById(R.id.test1e);
        Test2es = (ImageView) findViewById(R.id.test2e);
        Test3es = (ImageView) findViewById(R.id.test3e);
        //List of Model
        Listmodel = db.getAllRecordFromModel();
        // Log.e("TAG List Size1.1 : ",Listmodel.size()+"" );
        // Log.e("TAG List q1 : ",Listmodel.get(0).Question+"" );
        // Log.e("TAG List q2 : ",Listmodel.get(1).Question+"" );
        //Log.e("TAG List q3 : ",Listmodel.get(2).Question+"" );
        Tableau = (LinearLayout) findViewById(R.id.Tableau);
        anim = AnimationUtils.loadAnimation(this,R.anim.scale_up);
        Tableau.setAnimation(anim);
        Questions();
        MobileAds.initialize(MainSouraActivity.this, "ca-app-pub-9961705383410701~5674929633");
        myAdViewer = MobileAds.getRewardedVideoAdInstance(this);
        myAdViewer.setRewardedVideoAdListener(MainSouraActivity.this);
        loadRewardedVideoAd();
    }

    public void btn_homeX(View view) {
        finish();
        MainSouraActivity.this.finish();
    }

    public void Questions() {
        //Log.e("TAG i", i+"" );
        // nawModel = Listmodel.get(i);
        if ((!Listmodel.isEmpty()) || (Listmodel.size() > 0)) {
            nawModel = Listmodel.get(i);
        }

        sizeAilt = Listmodel.size() - 1;
        nawModel0=Listmodel.get(RandomizePic());
        nawModel1 = Listmodel.get(0);
        nawModel2 = Listmodel.get(1);
        nawModel3 = Listmodel.get(2);
        nawModel4 = Listmodel.get(3);
        nawModel5 = Listmodel.get(4);
        nawModel6 = Listmodel.get(5);
        nawModel7 = Listmodel.get(6);
        nawModel8 = Listmodel.get(7);
        nawModel9 = Listmodel.get(8);
        nawModel10 = Listmodel.get(9);
        Questions.setText(nawModel.Question);
        Test1es.setImageResource(Integer.parseInt(nawModel.IDPic1));
        Test2es.setImageResource(Integer.parseInt(nawModel.IDPic2));
        Test3es.setImageResource(Integer.parseInt(nawModel.IDPic3));
        Test1es.setClickable(true);
        Test2es.setClickable(true);
        Test3es.setClickable(true);
        if (i == 0) {
            Questions.setVisibility(View.INVISIBLE);
            Test1es.setVisibility(View.INVISIBLE);
            Test2es.setVisibility(View.INVISIBLE);
            Test3es.setVisibility(View.INVISIBLE);
            MyPlayer = MediaPlayer.create(MainSouraActivity.this, R.raw.molaima);
            MyPlayer.start();
            MyTimer2 = new CountDownTimer(4500, 1000) {
                public void onTick(long millisUntilFinished) {
                    isRunning2 = true;
                }
                public void onFinish() {
                    isRunning2= false;
                    MyPlayer = MediaPlayer.create(MainSouraActivity.this, Integer.parseInt(nawModel.IDSoundV));
                    MyPlayer.start();
                    Questions.setVisibility(View.VISIBLE);
                    Test1es.setVisibility(View.VISIBLE);
                    Test2es.setVisibility(View.VISIBLE);
                    Test3es.setVisibility(View.VISIBLE);
                }
            }.start();
        } else {
            if (MyPlayer.isPlaying()) {
                MyPlayer.stop();
                MyPlayer.release();
            }
            MyPlayer = MediaPlayer.create(MainSouraActivity.this, Integer.parseInt(nawModel.IDSoundV));
            MyPlayer.start();
        }
        refrechCountDownTimer();
        try {
            Test1es.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (nawModel.IDPic1.equals(nawModel.IDPicV)) {
                        if (i<9) {Test1es.setClickable(false);  }
                        Test2es.setVisibility(View.INVISIBLE);
                        Test3es.setVisibility(View.INVISIBLE);
                        if (x<100)
                        {
                            if (MyPlayer.isPlaying()) {
                                MyPlayer.stop();
                                MyPlayer.release();
                            }
                            MyPlayer = MediaPlayer.create(MainSouraActivity.this,vrai[RandomizeVrai()]);
                            MyPlayer.start();
                        }
                        if (i == 0) {
                            BoutonX1s.setVisibility(View.VISIBLE);
                            BoutonX1s.setImageResource(Integer.parseInt(nawModel1.IDPicV));
                        }
                        if (i == 1) {
                            BoutonX2s.setVisibility(View.VISIBLE);
                            BoutonX2s.setImageResource(Integer.parseInt(nawModel2.IDPicV));
                        }
                        if (i == 2) {
                            BoutonX3s.setVisibility(View.VISIBLE);
                            BoutonX3s.setImageResource(Integer.parseInt(nawModel3.IDPicV));
                        }
                        if (i == 3) {
                            BoutonX4s.setVisibility(View.VISIBLE);
                            BoutonX4s.setImageResource(Integer.parseInt(nawModel4.IDPicV));
                        }
                        if (i == 4) {
                            BoutonX5s.setVisibility(View.VISIBLE);
                            BoutonX5s.setImageResource(Integer.parseInt(nawModel5.IDPicV));
                        }
                        if (i == 5) {
                            BoutonX6s.setVisibility(View.VISIBLE);
                            BoutonX6s.setImageResource(Integer.parseInt(nawModel6.IDPicV));
                        }
                        if (i == 6) {
                            BoutonX7s.setVisibility(View.VISIBLE);
                            BoutonX7s.setImageResource(Integer.parseInt(nawModel7.IDPicV));
                        }
                        if (i == 7) {
                            BoutonX8s.setVisibility(View.VISIBLE);
                            BoutonX8s.setImageResource(Integer.parseInt(nawModel8.IDPicV));
                        }
                        if (i == 8) {
                            BoutonX9s.setVisibility(View.VISIBLE);
                            BoutonX9s.setImageResource(Integer.parseInt(nawModel9.IDPicV));
                        }
                        if (i == 9) {
                            BoutonX10s.setVisibility(View.VISIBLE);
                            BoutonX10s.setImageResource(Integer.parseInt(nawModel10.IDPicV));
                        }
                        if ((i == sizeAilt) && (x==100)) {
                            Test1es.setClickable(false);
                            MyTimer3 =  new CountDownTimer(6000, 1000) {
                                public void onTick ( long millisUntilFinished){
                                    isRunning3 = true;
                                }

                                public void onFinish () {
                                    isRunning3 = false;
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);
                                }
                            }.start();
                        }
                        if (i < sizeAilt) {
                            MyTimer4 = new CountDownTimer(3000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    isRunning4 = true;
                                }

                                public void onFinish() {
                                    isRunning4= false;
                                    progress_bar.setProgress(0);
                                    progress_bar.setSecondaryProgress(10);
                                    h = 0;
                                    i++;
                                    Questions();
                                    Test1es.setClickable(true);
                                    Test2es.setVisibility(View.VISIBLE);
                                    Test3es.setVisibility(View.VISIBLE);
                                }
                            }.start();
                        }

                    } else {
                        if (MyPlayer.isPlaying()) {
                            MyPlayer.stop();
                            MyPlayer.release();
                        }
                        MyPlayer = MediaPlayer.create(MainSouraActivity.this,faux[Randomizefaux()]);
                        MyPlayer.start();
                    }
                }
            });
        } catch (Exception ex1) {
            ex1.getMessage();
        }
        try {

            Test2es.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (nawModel.IDPic2.equals(nawModel.IDPicV)) {
                        Test1es.setVisibility(View.INVISIBLE);
                        if (i<9) {Test2es.setClickable(false);}
                        Test3es.setVisibility(View.INVISIBLE);
                        if (x<100)
                        {
                            if (MyPlayer.isPlaying()) {
                                MyPlayer.stop();
                                MyPlayer.release();
                            }
                            MyPlayer = MediaPlayer.create(MainSouraActivity.this,vrai[RandomizeVrai()]);
                            MyPlayer.start();
                        }
                        if (i == 0) {
                            BoutonX1s.setVisibility(View.VISIBLE);
                            BoutonX1s.setImageResource(Integer.parseInt(nawModel1.IDPicV));
                        }
                        if (i == 1) {
                            BoutonX2s.setVisibility(View.VISIBLE);
                            BoutonX2s.setImageResource(Integer.parseInt(nawModel2.IDPicV));
                        }
                        if (i == 2) {
                            BoutonX3s.setVisibility(View.VISIBLE);
                            BoutonX3s.setImageResource(Integer.parseInt(nawModel3.IDPicV));
                        }
                        if (i == 3) {
                            BoutonX4s.setVisibility(View.VISIBLE);
                            BoutonX4s.setImageResource(Integer.parseInt(nawModel4.IDPicV));
                        }
                        if (i == 4) {
                            BoutonX5s.setVisibility(View.VISIBLE);
                            BoutonX5s.setImageResource(Integer.parseInt(nawModel5.IDPicV));
                        }
                        if (i == 5) {
                            BoutonX6s.setVisibility(View.VISIBLE);
                            BoutonX6s.setImageResource(Integer.parseInt(nawModel6.IDPicV));
                        }
                        if (i == 6) {
                            BoutonX7s.setVisibility(View.VISIBLE);
                            BoutonX7s.setImageResource(Integer.parseInt(nawModel7.IDPicV));
                        }
                        if (i == 7) {
                            BoutonX8s.setVisibility(View.VISIBLE);
                            BoutonX8s.setImageResource(Integer.parseInt(nawModel8.IDPicV));
                        }
                        if (i == 8) {
                            BoutonX9s.setVisibility(View.VISIBLE);
                            BoutonX9s.setImageResource(Integer.parseInt(nawModel9.IDPicV));
                        }
                        if (i == 9) {
                            BoutonX10s.setVisibility(View.VISIBLE);
                            BoutonX10s.setImageResource(Integer.parseInt(nawModel10.IDPicV));
                        }
                        if ((i == sizeAilt) && (x==100)) {
                            Test2es.setClickable(false);
                            MyTimer5= new CountDownTimer(6000, 1000) {
                                public void onTick ( long millisUntilFinished){
                                    isRunning5 = true;
                                }

                                public void onFinish () {
                                    isRunning5= false;
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);

                                }
                            }.start();
                        }
                        if (i < sizeAilt) {
                            MyTimer6 = new CountDownTimer(3000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    isRunning6 = true;
                                }

                                public void onFinish() {
                                    isRunning6= false;
                                    progress_bar.setProgress(0);
                                    progress_bar.setSecondaryProgress(10);
                                    h = 0;
                                    i++;
                                    Questions();
                                    Test1es.setVisibility(View.VISIBLE);
                                    Test2es.setClickable(true);
                                    Test3es.setVisibility(View.VISIBLE);
                                }
                            }.start();
                        }
                    } else {
                        if (MyPlayer.isPlaying()) {
                            MyPlayer.stop();
                            MyPlayer.release();
                        }
                        MyPlayer = MediaPlayer.create(MainSouraActivity.this,faux[Randomizefaux()]);
                        MyPlayer.start();
                    }

                }
            });
        } catch (Exception ex2) {
            ex2.getMessage();
        }
        try {
            Test3es.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (nawModel.IDPic3.equals(nawModel.IDPicV)) {
                        Test1es.setVisibility(View.INVISIBLE);
                        Test2es.setVisibility(View.INVISIBLE);
                        if (i<9) {Test3es.setClickable(false);}
                        if (x < 100) {
                            if (MyPlayer.isPlaying()) {
                                MyPlayer.stop();
                                MyPlayer.release();
                            }
                            MyPlayer = MediaPlayer.create(MainSouraActivity.this,vrai[RandomizeVrai()]);
                            MyPlayer.start();
                        }
                        if (i == 0) {
                            BoutonX1s.setVisibility(View.VISIBLE);
                            BoutonX1s.setImageResource(Integer.parseInt(nawModel1.IDPicV));
                        }
                        if (i == 1) {
                            BoutonX2s.setVisibility(View.VISIBLE);
                            BoutonX2s.setImageResource(Integer.parseInt(nawModel2.IDPicV));
                        }
                        if (i == 2) {
                            BoutonX3s.setVisibility(View.VISIBLE);
                            BoutonX3s.setImageResource(Integer.parseInt(nawModel3.IDPicV));
                        }
                        if (i == 3) {
                            BoutonX4s.setVisibility(View.VISIBLE);
                            BoutonX4s.setImageResource(Integer.parseInt(nawModel4.IDPicV));
                        }
                        if (i == 4) {
                            BoutonX5s.setVisibility(View.VISIBLE);
                            BoutonX5s.setImageResource(Integer.parseInt(nawModel5.IDPicV));
                        }
                        if (i == 5) {
                            BoutonX6s.setVisibility(View.VISIBLE);
                            BoutonX6s.setImageResource(Integer.parseInt(nawModel6.IDPicV));
                        }
                        if (i == 6) {
                            BoutonX7s.setVisibility(View.VISIBLE);
                            BoutonX7s.setImageResource(Integer.parseInt(nawModel7.IDPicV));
                        }
                        if (i == 7) {
                            BoutonX8s.setVisibility(View.VISIBLE);
                            BoutonX8s.setImageResource(Integer.parseInt(nawModel8.IDPicV));
                        }
                        if (i == 8) {
                            BoutonX9s.setVisibility(View.VISIBLE);
                            BoutonX9s.setImageResource(Integer.parseInt(nawModel9.IDPicV));
                        }
                        if (i == 9) {
                            BoutonX10s.setVisibility(View.VISIBLE);
                            BoutonX10s.setImageResource(Integer.parseInt(nawModel10.IDPicV));
                        }
                        if ((i == sizeAilt) && (x==100)) {
                            Test3es.setClickable(false);
                            MyTimer7 = new CountDownTimer(6000, 1000) {
                                public void onTick ( long millisUntilFinished){
                                    isRunning7 = true;
                                }

                                public void onFinish () {
                                    isRunning7= false;
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);
                                }
                            }.start();
                        }
                        if (i < sizeAilt) {
                            MyTimer8 = new CountDownTimer(3000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    isRunning8 = true;
                                }

                                public void onFinish() {
                                    isRunning8= false;
                                    progress_bar.setProgress(0);
                                    progress_bar.setSecondaryProgress(10);
                                    h = 0;
                                    i++;
                                    Questions();
                                    Test1es.setVisibility(View.VISIBLE);
                                    Test2es.setVisibility(View.VISIBLE);
                                    Test3es.setClickable(true);
                                }
                            }.start();
                        }
                    } else {
                        if (MyPlayer.isPlaying()) {
                            MyPlayer.stop();
                            MyPlayer.release();
                        }
                        MyPlayer = MediaPlayer.create(MainSouraActivity.this,faux[Randomizefaux()]);
                        MyPlayer.start();

                    }

                }
            });
        } catch (Exception ex3) {
            ex3.getMessage();
        }
    }

    public void refrechCountDownTimer() {
        if( (!Listmodel.isEmpty()) || (Listmodel.size() > 0) )
        {
            nawModel= Listmodel.get(i);
        }
        sizeAilt = Listmodel.size() - 1;
        nawModel0 =Listmodel.get(RandomizePic());
        nawModel1 = Listmodel.get(0);
        nawModel2 = Listmodel.get(1);
        nawModel3 = Listmodel.get(2);
        nawModel4 = Listmodel.get(3);
        nawModel5 = Listmodel.get(4);
        nawModel6 = Listmodel.get(5);
        nawModel7 = Listmodel.get(6);
        nawModel8 = Listmodel.get(7);
        nawModel9 = Listmodel.get(8);
        nawModel10 = Listmodel.get(9);
        //try CountDownTimer
        MyTimer1 = new CountDownTimer(6000000, 1000) {
            public void onTick(long millisUntilFinished) {
                isRunning1 = true;
                h++;
                if (h == 2) {
                    x =  progress_bar.getProgress();
                    y = progress_bar.getSecondaryProgress();
                    x++;
                    y++;
                    progress_bar.setProgress(x);
                    progress_bar.setSecondaryProgress(y);
                    if ((x < 100) || (y<100))  {
                        h = 0;
                    }
                    if (x == 100) {
                        if (i<9)
                        {
                            if (MyPlayer.isPlaying()) {
                                MyPlayer.stop();
                                MyPlayer.release();
                            }
                            MyPlayer = MediaPlayer.create(MainSouraActivity.this, R.raw.vraispant);
                            MyPlayer.start();
                        }
                        if (i==9) {
                            if (MyPlayer.isPlaying()) {
                                MyPlayer.stop();
                                MyPlayer.release();
                            }
                            MyPlayer = MediaPlayer.create(MainSouraActivity.this, R.raw.vraishams);
                            MyPlayer.start();
                        }
                        //model1 IDPIC3
                        if (nawModel.IDPic3.equals(nawModel.IDPicV)) {
                            Test1es.setVisibility(View.INVISIBLE);
                            Test2es.setVisibility(View.INVISIBLE);
                            if (i == 0) {
                                BoutonX1s.setVisibility(View.VISIBLE);
                                BoutonX1s.setImageResource(Integer.parseInt(nawModel1.IDPicV));
                            }
                            if (i == 1) {
                                BoutonX2s.setVisibility(View.VISIBLE);
                                BoutonX2s.setImageResource(Integer.parseInt(nawModel2.IDPicV));
                            }
                            if (i == 2) {
                                BoutonX3s.setVisibility(View.VISIBLE);
                                BoutonX3s.setImageResource(Integer.parseInt(nawModel3.IDPicV));
                            }
                            if (i == 3) {
                                BoutonX4s.setVisibility(View.VISIBLE);
                                BoutonX4s.setImageResource(Integer.parseInt(nawModel4.IDPicV));
                            }
                            if (i == 4) {
                                BoutonX5s.setVisibility(View.VISIBLE);
                                BoutonX5s.setImageResource(Integer.parseInt(nawModel5.IDPicV));
                            }
                            if (i == 5) {
                                BoutonX6s.setVisibility(View.VISIBLE);
                                BoutonX6s.setImageResource(Integer.parseInt(nawModel6.IDPicV));
                            }
                            if (i == 6) {
                                BoutonX7s.setVisibility(View.VISIBLE);
                                BoutonX7s.setImageResource(Integer.parseInt(nawModel7.IDPicV));
                            }
                            if (i == 7) {
                                BoutonX8s.setVisibility(View.VISIBLE);
                                BoutonX8s.setImageResource(Integer.parseInt(nawModel8.IDPicV));
                            }
                            if (i == 8) {
                                BoutonX9s.setVisibility(View.VISIBLE);
                                BoutonX9s.setImageResource(Integer.parseInt(nawModel9.IDPicV));
                            }
                            if (i == 9) {
                                BoutonX10s.setVisibility(View.VISIBLE);
                                BoutonX10s.setImageResource(Integer.parseInt(nawModel10.IDPicV));
                            }
                        }
                        //fin model IDPIC3
                        // debut model IDPIC2
                        if (nawModel.IDPic2.equals(nawModel.IDPicV)) {
                            Test1es.setVisibility(View.INVISIBLE);
                            Test3es.setVisibility(View.INVISIBLE);
                            if (i == 0) {
                                BoutonX1s.setVisibility(View.VISIBLE);
                                BoutonX1s.setImageResource(Integer.parseInt(nawModel1.IDPicV));
                            }
                            if (i == 1) {
                                BoutonX2s.setVisibility(View.VISIBLE);
                                BoutonX2s.setImageResource(Integer.parseInt(nawModel2.IDPicV));
                            }
                            if (i == 2) {
                                BoutonX3s.setVisibility(View.VISIBLE);
                                BoutonX3s.setImageResource(Integer.parseInt(nawModel3.IDPicV));
                            }
                            if (i == 3) {
                                BoutonX4s.setVisibility(View.VISIBLE);
                                BoutonX4s.setImageResource(Integer.parseInt(nawModel4.IDPicV));
                            }
                            if (i == 4) {
                                BoutonX5s.setVisibility(View.VISIBLE);
                                BoutonX5s.setImageResource(Integer.parseInt(nawModel5.IDPicV));
                            }
                            if (i == 5) {
                                BoutonX6s.setVisibility(View.VISIBLE);
                                BoutonX6s.setImageResource(Integer.parseInt(nawModel6.IDPicV));
                            }
                            if (i == 6) {
                                BoutonX7s.setVisibility(View.VISIBLE);
                                BoutonX7s.setImageResource(Integer.parseInt(nawModel7.IDPicV));
                            }
                            if (i == 7) {
                                BoutonX8s.setVisibility(View.VISIBLE);
                                BoutonX8s.setImageResource(Integer.parseInt(nawModel8.IDPicV));
                            }
                            if (i == 8) {
                                BoutonX9s.setVisibility(View.VISIBLE);
                                BoutonX9s.setImageResource(Integer.parseInt(nawModel9.IDPicV));
                            }
                            if (i == 9) {
                                BoutonX10s.setVisibility(View.VISIBLE);
                                BoutonX10s.setImageResource(Integer.parseInt(nawModel10.IDPicV));
                            }
                        }
                        //fin model IDPIC2
                        // debut model IDPIC1
                        if (nawModel.IDPic1.equals(nawModel.IDPicV)) {
                            Test2es.setVisibility(View.INVISIBLE);
                            Test3es.setVisibility(View.INVISIBLE);
                            if (i == 0) {
                                BoutonX1s.setVisibility(View.VISIBLE);
                                BoutonX1s.setImageResource(Integer.parseInt(nawModel1.IDPicV));
                            }
                            if (i == 1) {
                                BoutonX2s.setVisibility(View.VISIBLE);
                                BoutonX2s.setImageResource(Integer.parseInt(nawModel2.IDPicV));
                            }
                            if (i == 2) {
                                BoutonX3s.setVisibility(View.VISIBLE);
                                BoutonX3s.setImageResource(Integer.parseInt(nawModel3.IDPicV));
                            }
                            if (i == 3) {
                                BoutonX4s.setVisibility(View.VISIBLE);
                                BoutonX4s.setImageResource(Integer.parseInt(nawModel4.IDPicV));
                            }
                            if (i == 4) {
                                BoutonX5s.setVisibility(View.VISIBLE);
                                BoutonX5s.setImageResource(Integer.parseInt(nawModel5.IDPicV));
                            }
                            if (i == 5) {
                                BoutonX6s.setVisibility(View.VISIBLE);
                                BoutonX6s.setImageResource(Integer.parseInt(nawModel6.IDPicV));
                            }
                            if (i == 6) {
                                BoutonX7s.setVisibility(View.VISIBLE);
                                BoutonX7s.setImageResource(Integer.parseInt(nawModel7.IDPicV));
                            }
                            if (i == 7) {
                                BoutonX8s.setVisibility(View.VISIBLE);
                                BoutonX8s.setImageResource(Integer.parseInt(nawModel8.IDPicV));
                            }
                            if (i == 8) {
                                BoutonX9s.setVisibility(View.VISIBLE);
                                BoutonX9s.setImageResource(Integer.parseInt(nawModel9.IDPicV));
                            }
                            if (i == 9) {
                                BoutonX10s.setVisibility(View.VISIBLE);
                                BoutonX10s.setImageResource(Integer.parseInt(nawModel10.IDPicV));
                            }
                        }
                        //fin model IDPIC1

                    }
                }
            }

            public void onFinish() {
                isRunning1 = false;
            }
        }.start();
    }

    public void setting(View view) {

    }
    public int RandomizeVrai() {
        Random r = new Random();
        int Low = 0;
        int High = 2;
        int rndm = r.nextInt(High-Low) + Low;
        return rndm;
    }
    public int Randomizefaux() {
        Random r = new Random();
        int Low = 0;
        int High = 4;
        int rndm = r.nextInt(High-Low) + Low;
        return rndm;
    }
    public int RandomizePic() {
        Random r = new Random();
        int Low = 0;
        int High = 10;
        int rndm = r.nextInt(High-Low) + Low;
        return rndm;
    }
    @Override
    public void onRewardedVideoAdLoaded() {
        if (myAdViewer.isLoaded()) {
            myAdViewer.show();
        }    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        nawModel0=Listmodel.get(RandomizePic());
        final Dialog dialog2 = new Dialog(this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog2.setContentView(R.layout.cadeau);
        Button Closebtn = (Button) dialog2.findViewById(R.id.Cls);
        Closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.setCancelable(false);
        dialog2.show();
        QuestionsDialog = (TextView) dialog2.findViewById(R.id.Question);
        TesTezDialog = (ImageView) dialog2.findViewById(R.id.IMGDialog);
        QuestionsDialog.setText(nawModel0.Question);
        TesTezDialog.setImageResource(Integer.parseInt(nawModel0.IDPicV));

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        // Toast.makeText(this, "الإعلان فشل في التحميل", Toast.LENGTH_SHORT).show();

    }
    private void loadRewardedVideoAd() {
        if(!myAdViewer.isLoaded())
        {
            myAdViewer.loadAd("ca-app-pub-9961705383410701/5720691312", new AdRequest.Builder().build());
        }
    }
}
