package com.abdeljalilkchih.learnarabicforkids;

/**
 * Created by HP on 21/10/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 16/10/2017.
 */

public class MyDbConnexion extends SQLiteOpenHelper {
    public static final String MyDBname = "Mytest.db";
    public static final int Version = 1;
    public static final String MyTable = "`SouraMain`";

    public MyDbConnexion(Context context)
    {
        super(context, MyDBname, null, Version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE table IF NOT EXISTS " + MyTable + " (`ID` INTEGER PRIMARY KEY AUTOINCREMENT " +
                ",`Question` INTEGER ,`IDPic1` INTEGER, `IDPic2` INTEGER,`IDPic3` INTEGER,`IDPicV` INTEGER,`IDSoundV` INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MyTable);
        onCreate(db);
    }

    public boolean insertData(String Question, int IDPic1, int IDPic2, int IDPic3, int IDPicV,int IDSoundV){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues myContentValue = new ContentValues();
        myContentValue.put("Question", Question);
        myContentValue.put("IDPic1", IDPic1);
        myContentValue.put("IDPic2", IDPic2);
        myContentValue.put("IDPic3", IDPic3);
        myContentValue.put("IDPicV", IDPicV);
        myContentValue.put("IDSoundV", IDSoundV);
        long result =  db.insert(MyTable, null, myContentValue);

        if(result == -1)
            return false;
        else
            return  true;
    }
    public List<Model> getAllRecordFromModel() {

        List<Model> arrList = new ArrayList<>();
        //     arrList = null;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("SELECT * FROM "+MyTable, null);

        res.moveToFirst();
        while (res.isAfterLast() == false) {

            String rowId = res.getString(0);
            String rowQuestion = res.getString(1);
            String rowIDPic1 = res.getString(2);
            String rowIDPic2 = res.getString(3);
            String rowIDPic3 = res.getString(4);
            String rowIDPicV = res.getString(5);
            String rowIDSoundV = res.getString(6);


            Model model = new Model();

            model.ID = rowId;
            model.Question = rowQuestion;
            model.IDPic1 = rowIDPic1;
            model.IDPic2 = rowIDPic2;
            model.IDPic3 = rowIDPic3;
            model.IDPicV = rowIDPicV;
            model.IDSoundV = rowIDSoundV;


            arrList.add(model);

            res.moveToNext();

        }

        return arrList;
    }
}

