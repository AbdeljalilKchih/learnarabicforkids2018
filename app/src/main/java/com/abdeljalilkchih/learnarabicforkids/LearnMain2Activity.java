package com.abdeljalilkchih.learnarabicforkids;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class LearnMain2Activity extends AppCompatActivity  {
        private static final String TAG = "LearnMain2Activity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learn_main2);
        //... Display the Ads Déclaration here ...

        //The End
/// Go Record
        boolean firstrunData = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("firstrun", true);
        MyDbConnexion mydb = new MyDbConnexion(this);
        if (firstrunData) {
            mydb.insertData("جمل", R.drawable.jamal, R.drawable.namla, R.drawable.atrouss, R.drawable.jamal, R.raw.jamal);
            mydb.insertData("طبيب", R.drawable.rajel, R.drawable.tabib, R.drawable.shorti, R.drawable.tabib, R.raw.tabib);
            mydb.insertData("زرافة", R.drawable.assad, R.drawable.kalb, R.drawable.zarafa, R.drawable.zarafa, R.raw.kzarafa);
            mydb.insertData("بيضة", R.drawable.baida, R.drawable.laymoon, R.drawable.gofel, R.drawable.baida, R.raw.baida);
            mydb.insertData("خبز", R.drawable.assal, R.drawable.poulet_roti, R.drawable.khobz, R.drawable.khobz, R.raw.khobz);
            mydb.insertData("طائرة", R.drawable.tayara, R.drawable.warda, R.drawable.assfour, R.drawable.tayara, R.raw.tayara);
            mydb.insertData("جندي", R.drawable.malik, R.drawable.jondi, R.drawable.gerad, R.drawable.jondi, R.raw.jondi);
            mydb.insertData("تفاحة", R.drawable.genareya, R.drawable.mashmash, R.drawable.tofaha, R.drawable.tofaha, R.raw.tofaha);
            mydb.insertData("سروال", R.drawable.serwal, R.drawable.cadeau, R.drawable.sandoug, R.drawable.serwal, R.raw.serwal);
            mydb.insertData("شمس", R.drawable.gamra, R.drawable.shab, R.drawable.chams, R.drawable.chams, R.raw.chams);
        }
        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putBoolean("firstrun", false).commit();

            }

    public void Btn_Main(View view) {
        finish();
        LearnMain2Activity.this.finish();

    }

    public void Btn_horouf(View view) {
         Intent MyIntend2 = new Intent(this,houroufMain2Activity.class);
        startActivity(MyIntend2);
        finish();
        LearnMain2Activity.this.finish();

    }

    public void Btn_soura(View view) {
     Intent MyIntend3 = new Intent(this,MainSouraActivity.class);
        startActivity(MyIntend3);
        finish();
        LearnMain2Activity.this.finish();
    }
    public void Btn_kitaba(View view) {
        final Dialog dialog2 = new Dialog(this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog2.setContentView(R.layout.under_construction);
        Button Closebtn = (Button) dialog2.findViewById(R.id.Cls);
        Closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.setCancelable(false);
        dialog2.show();

    }

    public void Btn_kalima(View view) {
        final Dialog dialog2 = new Dialog(this, android.R.style.Theme_Holo_Dialog_NoActionBar);
        dialog2.setContentView(R.layout.under_construction);
        Button Closebtn = (Button) dialog2.findViewById(R.id.Cls);
        Closebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog2.dismiss();
            }
        });
        dialog2.setCancelable(false);
        dialog2.show();
    }


}
